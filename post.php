<?php

require_once("resources/php/connection.php");
require_once("resources/php/ImageUploader.php");
require_once("resources/php/functions.php");
require_once("resources/php/session.php");

if(!isset($user)) {
	$user = checkSession();

}

if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['post-submit'])) {
	/*
	 * handles the new/edited post and inserts it into the database.
	 */
	$post         = false;
	$edit         = false;
	$location     = "post.php";
	$insert       = "INSERT INTO post (";
	$insertValues = ") VALUES (";
	$update       = "UPDATE post SET ";

	if(isset($_POST['header']) && $_POST['header'] != "") {
		$insert       .= "header";
		$insertValues .= "'". $_POST['header']. "'";
		$update       .= "header='". $_POST['header']. "'";
		$post          = true;

	}

	if(isset($_POST['content']) && $_POST['content'] != "") {
		if($post) {
			$insert       .= ", ";
			$insertValues .= ", ";
			$update       .= ", ";

		} else {
			$post          = true;

		}

		$insert       .= "content";
		$insertValues .= "'". $_POST['content']. "'";
		$update       .= "content='". $_POST['content']. "'";

	}

	if(isset($_POST['post_id'])) {
		if($post) {
			$insert       .= ", ";
			$insertValues .= ", ";
			$update       .= ", ";

		} else {
			$post          = true;

		}

		$insert       .= "post_id";
		$insertValues .= "'". $_POST['post_id']. "'";
		$update       .= "post_id='". $_POST['post_id']. "'";

	}

	if(isset($_POST['id']) && $_POST['id'] != -1) {
		$edit    = true;
		$update .= " WHERE id=". $_POST['id'];
		$id      = $_POST['id'];

	}
	
	if($edit) {
		DBContent($update,false);
		$location .= "?do=view&id=$id";

	} else if($post) {
		DBContent($insert. ", user_id". $insertValues. ", ". $user['login_id']. ")", false);
		$id        = DBContent("SELECT id FROM post WHERE user_id={$user['login_id']} ORDER BY id DESC LIMIT 1")[0]['id'];
		$location .= "?do=view&id=$id";

	} else {
		//not enough data, not sure what to do here.
		$location  = "index.php?fail=something%20went%20wrong";

	}

	//save image
	$file = isset($_FILES['file']) ? $_FILES['file'] : null;
	if(isset($_FILES['file']) && !$file['error']) {
		uploadImage($file,$id, "resources/img/post/preview/");
		//upload original
		move_uploaded_file($_FILES['file']['tmp_name'], "resources/img/post/" . $id. substr($_FILES['file']["name"],strrpos($_FILES['file']["name"], '.')));
	
	}

	header("Location: ". $location);//location
	die;

} else {
	$action = filter_input(INPUT_GET, "do");
	$id     = filter_input(INPUT_GET, "id");

	if(is_numeric($id) && substr($id,0,2) != '0x') {
		if($action == "view") {
			getPost($id, $user);

		} else if($action == "edit") {
			edit($id, $user);

		} else if($action == "remove") {
			$post = DBContent("SELECT * FROM post WHERE id=$id");
			if($post['user_id'] == $user['login_id']) {
				DBContent("DELETE FROM post WHERE id=$id", false);
				DBContent("DELETE FROM post WHERE post_id=$id", false);

			}
			//post removed, returning to currentpage?
			if(currentPage() != "post" && currentPage() != "index") {
				print("<script>alert('post removed, redirecting to ". currentPage(). "');</script>");
				header("Location: ". currentpage(). ".php");

			} else {
				print("<script>alert('post removed, redirecting to home');</script>");
				header("Location: index.php");

			}

		} else if($action = null) {
			print("This page needs some action");

		} else {
			print("No such action.");

		}

	}
}

//-----------------------------------------------------------
function edit($id, $user) {
	$data = DBContent("SELECT * FROM post WHERE id='$id'")[0];
	if($user['login_id'] != $data['user_id']) {
		//error, wrong user;
		header("Location: index.php");
		die;

	}
	postFormPrint($data);

}

function getPost($id, $user) {
	$data = DBContent("SELECT * FROM post WHERE id='$id'")[0];
	postPrint($data, $user);

}

function postFormPrint($data = null) {
	$id      = -1;
	$header  = "";
	$content = "";

	if(isset($data)) {
		$id       = $data['id'];
		$header   = $data['header'];
		$content  = $data['content'];
		$postFile = @glob("/resources/img/post/". $id. ".*")[0];
		htmlStart();
		htmlBody();
		require('resources/php/header.php');

	}
	print("<form method='POST' enctype='multipart/form-data'>" . "\n" .
	"\t" . "<input type='text' name='id' value='$id' hidden='hidden'></input>" . "\n" .
	"\t" . "<input type='text' limit='255' name='header' placeholder='Header' value='". $header. "'></input>" . "\n");

	if(isset($postFile)) {
		print("<img src='$postFile'></img>");

	}

	print("\t" . "Image  <input type='file' name='file'></input>" . "\n" .
	"\t" . "<textarea type='text' limit='400' name='content' placeholder='Content'>$content</textarea>" . "\n" .
	"\t" . "<input type='submit' name='post-submit' value='submit'></input>" . "\n" .
	"</form>");

	if(isset($data)) {
		htmlEnd();

	}

}

function postPrint($data, $user) {
	htmlStart();
		require('resources/php/head.php');
	htmlBody();
		require('resources/php/header.php');
		previewPost($data, $user);
		listComments($data['id'],$user);

		if($user) {
			print("<form method='POST' enctype='multipart/form-data'>" . "\n" .
			"\t" . "<input type='text' name='post_id' value='". $data['id']. "' hidden='hidden'></input>" . "\n" .
			"\t" . "<textarea type='text' limit='400' name='content' placeholder='Comment'></textarea>" . "\n" .
			"\t" . "<input type='submit'></input>" . "\n" .
			"</form>");

		} else {
			print("you must be logged in to comment");

		}

	htmlEnd();

}

function listComments($id, $user) {
	$comments  = DBContent("SELECT * FROM post WHERE post_id=$id");
	$localUser = null;

	if(!empty($comments)) {
		foreach($comments as $comment) {
			$localUser = DBContent("SELECT login_id, name FROM user WHERE login_id={$comment['user_id']}")[0];

			print("<div class='comment'>");

				print("<div class='user'>");
					print("<span>{$localUser['name']}</span>");
					print("<span>{$comment['time']}</span>");
				print("</div>");

				print("<span>{$comment['content']}</span>");

			print("</div>");

		}

	}

}
function previewPost($data, $user) {
	$postFile   = @glob("resources/img/post/". $data['id']. ".*")[0];
	$localUser  = DBContent("SELECT * FROM user WHERE login_id={$data['user_id']}")[0];
	$localImage = @glob("resources/img/user/". $localUser['login_id']. ".*")[0];

	if(empty($localImage)) {
		$localImage = @glob("resources/img/user/default.png")[0];

	}

	print("<div class='post'>");
		print("<div class='user'>");
			print("<a href='profile.php?id={$localUser['login_id']}'>");
				print("<img src='$localImage'></img>");
				print("<span>{$localUser['name']}</span>");
			print("</a>");

			if($user && $data['user_id'] != $user['login_id']) {
				print("<div class='user-buttons'>");
					previewFavoriteFollow($data, $user);
				print("</div>");

			}

		print("</div>");

		print("<h1>". $data['header']. "</h1>");

		if($postFile) {
			print("<a class='imageLink' href='$postFile'>");

				if(currentPage() == "post") {
					print("<img src='$postFile'></img>");

				} else {
					print("<img src='resources/img/post/preview/". $data['id']. ".png'></img>");

				}

			print("</a>");

		}

		print("<div class='content'>");
			print("<span class='info'>". $data['content']. "</span>");
			print("<span class='time'>". $data['time']. "</span>");

			print("<div class='button-container'>");
				if(currentPage() != "post") {
					print("<a class='view' href='post.php?do=view&id={$data['id']}'><button>Reply</button></a>");

				}
				if($data['user_id'] == $user['login_id']) {
					print("<a class='edit' href='post.php?do=edit&id={$data['id']}'><button>Edit</button></a>");
					print("<a class='remove' href='post.php?do=remove&id={$data['id']}'><button>Remove</button></a>");
				}
			print("</div>");

		print("</div>");
	print("</div>");
}

/**
 *	A followup to previewPost, this shows two buttons that either if you are following the user of have favorited the post
 */
function previewFavoriteFollow($data, $user) {
	$favorite = DBContent("SELECT * FROM favorite WHERE u_id={$user['login_id']} AND fav_id={$data['id']}")[0];
	$followed = DBContent("SELECT * FROM follow WHERE u_id={$user['login_id']} AND follow_id={$data['user_id']}")[0];

	if($favorite) {
		$favimage = "filled-heart";

	} else {
		$favimage = "heart";

	}

	if($followed) {
		$follow     = "follow-active";
		$followText = "Following";

	} else {
		$follow     = "follow-inactive";
		$followText = "Follow";

	}

	print("<button class='follow $follow' value='{$data['user_id']}'>$followText</button>");
	print("<button class='favorite' value='{$data['id']}'><img src='resources/img/$favimage.png'></img></button>");
	
}
