<?php
require_once("resources/php/connection.php");
require_once("resources/php/functions.php");
require_once("post.php");

$limit = 10;
$page  = 0;
$max   = 0;
//paginator
$page  = filter_input(INPUT_GET, "page");
$max   = DBContent("SELECT count(*) AS number FROM post WHERE post_id IS null")[0]['number'];
$max  -= 1;
$max   = floor($max/$limit);

if($page) {
	setPage($page,$max);

}

htmlStart();
	require("resources/php/head.php");
htmlBody();
	require("resources/php/header.php");
	print("<main role='main'>");

		if($user) {
			print("<div id='poster'>");
				postFormPrint();
			print("</div>");

		}

		print("<div class='posts'>");
			paginator($page, $max);

			$data = DBContent("SELECT * FROM post WHERE post_id IS null ORDER BY id DESC LIMIT $limit OFFSET ". ($page*$limit));

			for($i = 0;$i < count($data); $i++) {
				previewPost($data[$i],$user);

			}

			paginator($page, $max);
		print("</div>");
		require("resources/php/follow.php");
	print("</main>");
	require("resources/php/footer.php");
htmlEnd();

/*
functionallity left:

favorite list (post-turnin)
remove/edit comment (post-turnin)
redo post.php (post-turnin)
design
profile.php show the posts you have commented in (post-turnin)
make most of the things classes. (post-turnin)



RULES: 
use "" for everything unless it's html attribute or an array variable.
"function () {

}"
use print instead of echo.
order function by names.
indent prints/functions depending on htmlcode inside.
use comments on logical functions, use "*docs" on more "advanced" functions.
one space between the functions
*/