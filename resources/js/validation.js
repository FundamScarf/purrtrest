window.onload = function() {
	document.forms[0].addEventListener("submit", validate);
	
};

function validate(e) {
	e.preventDefault();
	document.getElementsByTagName("span")[0].innerHTML = "";

	var mail         = e.target['mail'].value;
	var password     = e.target['pwd'].value;

	var passwordPass = validatePassword(password);
	var mailPass     = validateMail(mail);

	if(mailPass && passwordPass) {
		e.submit();

	}

}

function validateMail(mail) {
	var mailCheck = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

	if(mail.match(mailCheck)) {
		return true;

	}

	document.getElementsByTagName("span")[0].innerHTML += "Please enter a valid email address<br/>";

	return false;

}

function validatePassword(password) {
	var passCheck = /^(?=.*[A-Za-z0-9]).{4,20}$/;

	if(password.match(passCheck)) {
		return true;

	}

	document.getElementsByTagName("span")[0].innerHTML += "Please enter a password between 4 and 20 characters<br/>";

	return false;

}