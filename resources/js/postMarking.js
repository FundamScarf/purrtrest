window.onload = function() {
	var favorite = document.getElementsByClassName("favorite");
	var follow   = document.getElementsByClassName("follow");

	var i;

	for(i = 0; i < favorite.length;i++) {
		favorite[i].addEventListener('click', favoriteToggle);

	}
	for(i = 0; i < follow.length;i++) {
		follow[i].addEventListener('click', followToggle);

	}

}

function favoriteToggle(e) {
	toggleAjax("resources/php/favorite.php?id=" + e.target.value, function() {
		if(e.target.childNodes[0].getAttribute("src") == "resources/img/heart.png") {
			e.target.childNodes[0].setAttribute("src", "resources/img/filled-heart.png");

		} else {
			e.target.childNodes[0].setAttribute("src", "resources/img/heart.png");

		}

	});

}

function followToggle(e) {
	toggleAjax("resources/php/follow.php?id=" + e.target.value, function() {
		if(e.target.getAttribute('class') == "follow follow-active") {
			e.target.setAttribute('class', "follow follow-inactive");
			e.target.innerHTML = "Follow";

		} else {
			e.target.setAttribute('class', "follow follow-active");
			e.target.innerHTML = "Following";

		}

	});

}

function toggleAjax(url,callback) {
	var req    = new XMLHttpRequest();
	var method = "POST";

	req.onload = callback;
	req.open(method, url);
	req.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	req.send('toggle=true');
	
}