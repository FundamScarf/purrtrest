<?php

require_once("connection.php");
/*
* Behöver mer documentation
*/

/**
 * Här genererar vi en salt för våran lösenordskryptering, ska ej kallas
 */
function generateSalt() {
	$uniqueRandomString = md5(uniqid(mt_rand(), true));
	$base64String       = base64_encode($uniqueRandomString);
	$modBase64String    = str_replace("+", ".", $base64String);
	$salt               = substr($modBase64String, 0, 22);

	return $salt;

}

/**
 *	Här krypterar vi lösenordet, använd denna i er registreringssida.
 */
function passwordEncrypt($password) {
	$hashFormat = "$2y$10$";
	$salt       = generateSalt();
	$key        = $hashFormat . $salt;
	$hash       = crypt($password, $key);
	$hash       = md5($hash);

	return array('key' => $key, 'hash' => $hash);

}

/**
 *	validerar lösenordet, denna kallas av validateUser funktionen
 */
function validatePassword($input, $password, $key) {
	if(md5(crypt($input,$key)) == $password) {
		return true;

	}

	return false;

}

/*
*	Används för att slippa skriva det på varje sida
*/
function htmlStart() {
	print("<!DOCTYPE html>");
	print("<html>");
	print("<head>");

}

function htmlBody() {
	print("</head>");
	print("<body>");

}

function htmlEnd() {
	print("</body>");
	print("</html>");

}

/**
 *	@param $page shows what page you are currently on.
 *	@param $max the total number of pages
 */
function paginator($page, $max) {
	$gets = "";

	foreach($_GET as $key => $value){
		if($key != "page") {
			$gets .= "&". $key. "=". $value;

		}

	}

	//container
	print("<div class='paginator'>");

		//first page
		if($page == 0) {
			print("<span>first</span>");

		} else {
			print("<a href='?page=0$gets'>first</a>");

		}

		//previous
		if(($page-1) > 0) {
			print("<a href='?page=". ($page-1). $gets. "'>[". ($page-1). "]</a>");

		}

		//current
		if($page != 0 && $page != $max) {
			print("<span>[". ($page). "]</span>");

		}

		//next
		if(($page+1) < $max) {
			print("<a href='?page=". ($page+1). "". $gets. "'>[". ($page+1). "]</a>");

		}

		//last page
		if($page == $max) {
			print("<span>last</span>");

		} else {
			print("<a href='?page=". $max. $gets. "'>last</a>");

		}

	//end container
	print("</div>");

}
function currentPage() {
	$currentPage = substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
	
	return substr($currentPage,0,strpos($currentPage,"."));

}

function setPage(&$page, $max) {
	if(is_numeric($page) && substr($page,0,2) != "0x") {
		if($page > $max) {
			$page = $max;

		} else if($page < 0) {
			$page = 0;

		}

	} else {
		$page = 0;

	}

}