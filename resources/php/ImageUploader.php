<?php
function uploadImage($file, $id, $destination, $maxWidth = 320, $maxHeight = 320) {
	$name = $id;
	$tmp_name = $file["tmp_name"];
	$type     = substr($file["name"],strrpos($file["name"], '.'));
	$size     = $file["size"];
	$error    = $file["error"];

	$image = imageCreateFromAny($tmp_name);
	if ($image === false) {
		die ('Unable to open image');

	}

	$image = imageResize($image, $maxWidth, $maxHeight);
	imagepng($image, $destination . $name . ".png");

	//TODO delete old images that is not of the same type;

}

function imageResize($image, $maxWidth, $maxHeight) {
	$w = imagesx($image);
	$h = imagesy($image);

	if ($w > $h) {
		if ($w > $maxWidth) {
			$h *= $maxWidth / $w;
			$w = $maxWidth;

		}
		if ($h > $maxHeight) {
			$w *= $maxHeight / $h;
			$h = $maxHeight;

		}

	} else {
		if ($w > $maxWidth) {
			$h *= $maxWidth / $w;
			$w = $maxWidth;

		}

		if ($h > $maxHeight) {
			$w *= $maxHeight / $h;
			$h = $maxHeight;

		}

	}

	$image2 = ImageCreateTrueColor($w, $h);
	imagecopyResampled ($image2, $image, 0, 0, 0, 0, $w, $h, imagesx($image), imagesy($image));

	return $image2;

}

function imageCreateFromAny($filepath) {
	$type = getImageSize($filepath)[2];
	$allowedTypes = array(
		1,  // [] gif
        2,  // [] jpg
        3,  // [] png
        6   // [] bmp
    ); 

    if(!in_array($type,$allowedTypes)) {
    	return false;

    }

    switch($type) {
    	case 1:
    		$im = imageCreateFromGif($filepath);
    		break;
    	case 2:
    		$im = imageCreateFromJpeg($filepath);
    		break;
    	case 3:
    		$im = imageCreateFromPng($filepath);
    		break;
    	case 6:
    		$im = imageCreateFromBmp($filepath); 
    		break;

    }

    return $im;
    
}