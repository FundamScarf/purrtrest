<?php
require_once("session.php");
require_once("connection.php");
require_once("functions.php");

if(!isset($user)) {
	$user = checkSession();

}

if($_SERVER['REQUEST_METHOD'] == 'POST') {
	/*
	 *	set the user as followed/unfollowed by you.
	 */
	if($_POST['toggle']) {
		$id = $_GET['id'];

		if(is_numeric($id) && substr($id,0,2) != '0x') {
			//check
			$post = DBContent("SELECT * FROM follow WHERE follow_id = $id AND u_id = {$user['login_id']}")[0];

			if(!$post) {
				//add
				DBContent("INSERT INTO follow (u_id, follow_id) VALUES ({$user['login_id']},$id)",false);

			} else {
				//remove
				DBContent("DELETE FROM follow WHERE follow_id = $id AND u_id = {$user['login_id']}", false);

			}

		}

	}

} else {
	/*
	 *	show all you follow
	 */
	if($user) {
		$list = DBContent("SELECT * FROM follow WHERE u_id = {$user['login_id']}");

		if($list) {
			print("<div class='sub-list-wrapper'>");
			print("<div class='sub-list'>");
			print("<h1>Followed users</h1>");
			for($i = 0;$i < count($list); $i++) {
				$localUser  = DBContent("SELECT * FROM user WHERE login_id = {$list[$i]['follow_id']}")[0];
				$localImage = @glob("resources/img/user/". $localUser['login_id']. ".*")[0];

				if(!$localImage) {
					$localImage = @glob("resources/img/user/default.png")[0];

				}
				//should change to show how many followers they have?.
				print("<a href='profile.php?id={$localUser['login_id']}'><img src='$localImage'></img><span>{$localUser['name']}</span></a>");
			
			}

			print("</div>");
			print("</div>");

		}

	}
	
}