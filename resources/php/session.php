<?php
/*
 * Återanvänd kod som jag gjorde under en stuga
 */

require_once("connection.php");

/**
* varje gång du är inne på en sida som behöver veta ifall du är inloggad eller ej så kör denna funktion
* det returnerar en användare om någon är inloggad och "false" om ingen är inloggad
*/
function checkSession() {
	if (session_status() == PHP_SESSION_NONE) {
		session_start();

	}

	$sessionid = "";
	$sessionkey = "";

	//vi tilldelar en sessionsvairabel, om en sådan ej existerar så kollar vi cookies, om cookies ej finns så blir det en tom sträng
	if(isset($_SESSION['user']['id'])) {
		$sessionid = $_SESSION['user']['id'];

	} else {
		//eftersom vi ska kontakta databasen med denna så tar vi bort eventuella specialtecken
		$sessionid = filter_input(INPUT_COOKIE, "authid",FILTER_SANITIZE_SPECIAL_CHARS);
	
	}

	if(isset($_SESSION['user']['key'])) {
		$sessionkey = $_SESSION['user']['key'];

	} else {
		$sessionkey = filter_input(INPUT_COOKIE, "authkey");

	}

	//en annan enradsvariant är: $sessionid = if(isset($_SESSION['authid'])) ? $_SESSION['authid'] : filter_input(INPUT_COOKIE, "authid", FILTER_SANITIZE_SPECIAL_CHARS);
	
	//vi kollar ifall någon av dessa är tomma
	if($sessionid == "" || $sessionkey == "") {
		return false;

	}

	//vi gör en SELECT för att hämta användaren vi tror det är.
	$user = DBContent("SELECT * FROM user WHERE login_id = $sessionid");

	//om vi inte får en användare så tar vi bort sessionen och returnerar
	if(!$user) {
		resetSession($sessionid);
		return false;

	}
	//print($sessionkey);
	//print($user[0]['sessionkey']);

	//om vår lokala session nykel stämmer överens med vår användare så returnerar vi användaren
	if($sessionkey == $user[0]['sessionkey']) {
		return $user[0];

	} else {
		resetSession($sessionid);
		return false;

	}

}

/**
* Här skapar vi en session
*/
function createSession($id) {
	$length     = 42;//längden på vår sessionsnykel
	//vi gör en substr av en sträng blandare som tar det vi har skrivit in och skakar ihop dem
	$rawkey     = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
	$sessionkey = crypt($rawkey);//kryptera vår nykel så den blir mindre förståd för andra

	//vi uppdaterar nu användaren som logga in 
	if(DBContent("UPDATE user SET sessionkey='$sessionkey' WHERE login_id = '$id'", false)) {
		$_SESSION['user']['id'] = $id;
		$_SESSION['user']['key'] = $sessionkey;
		setcookie("authid", $id, time()+(60*60*24*30));
		setcookie("authkey", $sessionkey, time()+(60*60*24*30));

	}

}

/**
* här tar vi bort vår session
*/
function resetSession($id=-1) {
	if($id > 0) {
		DBContent("UPDATE user SET sessionkey='' WHERE login_id = '$id'", false);

	}
	
	setcookie("authid", '', 0);
	setcookie("authkey", '', 0);

	if(isset($_SESSION['user'])) {
		$_SESSION['user'] = null;

	}

}