<?php
require_once('session.php');

if(!isset($user)) {
	$user = checkSession();

}

print("<header>");

print("<a href='index.php'>Home</a>");

if($user) {
	$name = $user['name'];

	print("<a href='profile.php'>{$user['name']}</a>");
	print("<a href='logout.php?do=logout'>Logout</a>");

} else {
	print("<a href='login.php'>Login/Register</a>");

}

print("</header>");