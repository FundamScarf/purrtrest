<?php

define("SERVER", "localhost");
define("USERNAME", "root");
define("PASSWORD", "root");

/*
mysql

create table user (id INT auto_increment, username varchar(255), password varchar(255), hashkey varchar(255), sessionkey varchar(255), PRIMARY KEY(id));

*/

/*
*	Anväder oss av PDO (object) för att komma åt vår mysql server
* för att komma åt databasen(default="stugan") så kallar vi på denna funktion. 
*/
function DBConnect($db = "inlmn1") {
	$con = new PDO("mysql:host=" . SERVER . ";dbname=" . $db, USERNAME, PASSWORD);

	$con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	return $con;

}

/*
* För att göra kallningar till databasen för att antingen hämta eller lägga in data
* $get(default true) sätts beroende på vad du vill få tillbaka
* $query skriver du din fråga (SELECT, INSERT INTO, UPDATE osv.
*/
function DBContent($query, $get = true) {
	try {

		$con = DBConnect(); //skapa kontakt med databasen

		$stmt = $con->prepare($query); //gör queryobjectet till en egen variabel och kör den
		$stmt->execute();

		if ($get) {

			$array = $stmt->fetchAll(PDO::FETCH_ASSOC); //hämtar allt resultat från frågan och lägger in det i $array,
			//																						kan även göras via en while loop med while($stmt->fetch(PDO::FETCH_ASSOC) as $msg)

			if(!empty($array)) {
				return $array;

			} else {
				return false;

			}

		} else {
			if($stmt->rowCount() >= 1) {
				return true;

			}
			return false;

		}
	} catch (PDOException $e) {
		print($e->getMessage());//ifall något bilr fel så skriver den ut det, kolla igenom så inget blir fel innan du lägger upp den
	
	}

}