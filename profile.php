<?php

require_once("resources/php/connection.php");
require_once("resources/php/session.php");
require_once("resources/php/functions.php");
require_once("resources/php/ImageUploader.php");
require_once("post.php");

//user
if(!isset($user)) {
	$user = checkSession();

}

if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update-profile'])) {
	//user changeable
	$name = $_POST['name'];
	$file = isset($_FILES['file']) ? $_FILES['file'] : null;

	DBContent("UPDATE user SET name='$name' WHERE login_id={$user['login_id']}",false);

	if(isset($_FILES['file']) && !$file['error']) {
		uploadImage($file,$user['login_id'], "resources/img/user/");

	}

	header("Location: profile.php");

} else {
	//user
	$id = filter_input(INPUT_GET, "id");

	if(!$id || !is_numeric($id)) {
		$id = $user['login_id'];
	}

	//render
	htmlStart();
	require("resources/php/head.php");
	htmlBody();
	require("resources/php/header.php");
	print("<main role='main'>");
		//view profile, view posts
		viewProfile($id, $user);
		viewProfilePosts($id, $user);
		require("resources/php/follow.php");
	print("</main>");
	htmlEnd();

}

/**
 *	Shows the users profile (name and other stuff i dont have now).
 *	@param $id the id of the user you want to see.
 *	@param $user you, found from session.
 */
function viewProfile($id, $user) {
	$localUser = DBContent("SELECT * FROM user WHERE login_id=$id")[0];

	if(!$localUser) {
		$localUser = $user;

	}

	$postFile = @glob("resources/img/user/". $localUser['login_id']. ".*")[0];

	if(empty($postFile)) {
		$postFile = @glob("resources/img/user/default.*")[0];

	}

	print("<div class='profile'>");
		if($localUser['login_id'] == $user['login_id']) {
			print("<form method='POST' enctype='multipart/form-data'>");

			//image
			print("<img src='$postFile'></img>");
			print("<input type='file' name='file'></input>");

			//name
			print("<input name='name' value='{$user['name']}'></input>");
			print("<input type='submit' name='update-profile' value='update'></input>");

			print("</form>");

		} else {
			//image
			print("<img src='$postFile'></img>");

			//name
			print("<span>{$localUser['name']}</span>");

		}

	print("</div>");

}

/**
 *	shows the users posts, either be it you or the others
 *	@param $id the id of the user you want to see.
 *	@param $user you, found from session.
 */
function viewProfilePosts($id, $user) {
	$limit = 10;//could/should be a global variable
	$page  = 0;
	$max   = 0;
	//paginator
	$page  = filter_input(INPUT_GET, "page");
	$max   = DBContent("SELECT count(*) AS number FROM post WHERE post_id IS null AND user_id = $id")[0]['number'];
	$max  -= 1;
	$max   = floor($max/$limit);

	if($page) {
		setPage($page,$max);
		
	}

	$posts = DBContent("SELECT * from post WHERE post_id IS null AND user_id=$id ORDER BY id DESC LIMIT $limit OFFSET ". ($page*$limit));

	print("<div class='posts'>");
		paginator($page, $max);
		foreach($posts as $post) {
			previewPost($post,$user);

		}

		paginator($page, $max);
	print("</div>");

}