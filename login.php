<!-- Login v0.3 -->
<?php

require_once("resources/php/session.php");
require_once("resources/php/connection.php");
require_once("resources/php/functions.php");

if(!isset($user)) {
    $user = checkSession();
}

$action = filter_input(INPUT_GET, "do");

if($_SERVER["REQUEST_METHOD"] == "POST") {
    checkType();

}

if($action == "logout") {
    logout();

} else {//action is login/register
    $status = "Please enter a password between 4 and 20 characters";

    if(isset($_SESSION['status'])){
        $status = $_SESSION['status'];

    }

    htmlStart();
    require("resources/php/head.php");
        print("<script src='resources/js/validation.js'></script>");
    htmlBody();
        print("<main role='main' class='login'>");
            require("resources/form.html");

            print("<span>$status</span>");
        print("</main>");
    htmlEnd();

}
//-----------------------------------------

function logout() {
    $user = checkSession();

    if($user) {
        resetSession($user['id']);
        header("Location: index.php");
        die;

    }

}

/**
 *  check if it's a current user or a new one.
 */
function checkType() {
    $mail               = $_POST['mail'];
    $password           = $_POST['pwd'];
    $_SESSION['status'] = null;
    $success            = true;

    //validate mail
    if(!preg_match("/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/",$mail)) {
        $_SESSION['status'] .= "Please enter a valid email address<br/>";
        $success             = false;

    }

    //validate password
    if(!preg_match("/^(?=.*[A-Za-z0-9]).{4,20}$/",$password)) {
        $_SESSION['status'] .= "Please enter a password between 4 and 20 characters<br/>";
        $success             = false;

    }

    if($success) {
        $user = @DBContent("SELECT * FROM login where mail = '$mail'");

        if($user) {
            login($mail, $password);

        } else {
            register($mail,$password);

        }

    } else {
        $_SESSION['status'] .= "javascript validation failed";

    }

}

function login($mail, $password) {
    $user = validateUser($mail, $password);

    if($user) {
        //check for user
        checkUser($user['id']);
        createSession($user['id']);
        header('Location: index.php');
        die;

    }

}

/*
 *  validerar att det är rätt lösenord för användaren,
 *  framtidia tips är att låsa användaren efter x antal försök att logga in
 */
function validateUser($mail, $password) {
    $user      = DBContent("SELECT * FROM login where mail = '$mail'");
    $validPass = validatePassword($password,$user[0]['pass_hash'],$user[0]['pass_key']);

    if($validPass) {
        return $array = array('id' => $user[0]['id']);

    } else {
        return false;

    }

}

function checkUser($id) {
    if(!DBContent("SELECT * FROM user WHERE login_id = '$id'")) {
        DBContent("INSERT INTO user (login_id, name) VALUES ('$id', 'User$id')", false);

    }

}

/**
 *  register the user and automatically login the user.
 *  need to be worked on more laiter (autologin for one day and the you need to validate your mail)
 */
function register($mail,$password) {
    $uncryptPass = $password;
    $password    = passwordEncrypt($password);
    $passKey     = $password["key"];
    $passHash    = $password["hash"];

    DBContent("INSERT INTO login (mail,pass_key,pass_hash) VALUES ('$mail', '$passKey', '$passHash')", false);

    login($mail,$uncryptPass);
    
}